# Motivation and goals
When I work on some project I always create dedicated folder for it. 
I store some notes, useful links, picture, emails or other 
project supporting files. Sometimes I also store a local repo of this project. 
After some time, I got a lot of such project folders. It is difficult to 
find some information. 

The main aim was to create some textual summary of all projects in order to 
find some important notes or project filtered by given key words.

# How to use it
1. I recommend to create exe file using lunch4j. See my settings in: src/main/resources/lunch4j.
2. Add shortcut into TotalCommander:  
   * Command = path to exe file.
   * Parameters = %P%S
   * Icon = path to exe file.
3. Usage in TotalCommander
   * Select some folder.
   * Click on custom button.
   * html report file will be created inside this folder.

# Features
HTML report is created. It contains:
* filter task by words
* file structure for each task, including link to each file
* simple report creation by adding files into task folder:
  * _info.txt = insert task details like:
    * Jira number
    * Date
    * Author
    * Tags
    * Description
  * _details.txt = insert images or txt files as a details, including:
    * folder path
    * title
    * file name
    * picture size
  * !_exclude = put this file to a folder which shall be excluded in file tree.

Pictures of an example report:

![report_example_01.png](src/main/resources/documentation/report_example_01.png)