package com.michalrys.pronotes.pronotes;

import com.michalrys.pronotes.RunApp;
import com.michalrys.pronotes.pronotes.project.FolderProject;
import com.michalrys.pronotes.pronotes.project.Project;
import com.michalrys.pronotes.pronotes.project.details.ProjectDetails;
import com.michalrys.pronotes.pronotes.project.details.SimpleProjectDetailsForHtml;
import com.michalrys.pronotes.pronotes.project.info.ProjectInfo;
import com.michalrys.pronotes.pronotes.project.info.SimpleProjectInfo;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ProNotesHtmlSimpleTest {

    @Test
    public void shouldGenerateHtmlReport() {
        //given
        File projectsLocation = new File("./src/test/resources/dummyProjects");
        ProNotesHtmlSimple proNotes = new ProNotesHtmlSimple(projectsLocation);
        proNotes.findProjects();
        proNotes.generateReport();

        //when
        proNotes.writeReport();

        //then
        LocalDateTime dateTime = LocalDateTime.now();
        String fileName = dateTime.format(DateTimeFormatter.ofPattern("YYYY-MM-dd_HH-mm-ss"));
        File report = new File("./src/test/resources/dummyProjects/proNotes_" + fileName + ".html");

        Assert.assertTrue(report.exists());
    }

    @Test
    public void shouldFindProjects() {
        //given
        File projectsLocation = new File("./src/test/resources/dummyProjects");
        ProNotesHtmlSimple proNotes = new ProNotesHtmlSimple(projectsLocation);
        Project projectA = getExampleProjectA();

        //when
        List<Project> projects = proNotes.findProjects();
        //FIXME: temp print
        Project project1 = projects.get(0);
        System.out.println(projects);
        System.out.println("--- TITLE ---");
        System.out.println(project1.getTitle());
        System.out.println("--- INFO ---");
        System.out.println(project1.getProjectInfo());
        System.out.println("--- TREE ---");
        System.out.println(project1.getFileTree().getSimpleTree());
        System.out.println("--- DESCRIPTION ---");
        System.out.println(project1.getDescription());
        System.out.println("--- DETAILS ---");
        System.out.println(project1.getProjectDetails());
        System.out.println("-------------------");

        //then
        Assert.assertTrue(projects.contains(projectA));
        Assert.assertEquals(6, projects.size());
        for (Project project : projects) {
            if (project.equals(projectA)) {
                Assert.assertEquals(projectA.getDescription(), project.getDescription());
                Assert.assertEquals(projectA.getTitle(), project.getTitle());
                Assert.assertEquals(projectA.getProjectInfo().getJira(), project.getProjectInfo().getJira());
                Assert.assertEquals(projectA.getProjectInfo().getAuthor(), project.getProjectInfo().getAuthor());
                Assert.assertEquals(projectA.getProjectInfo().getDate(), project.getProjectInfo().getDate());
                Assert.assertEquals(projectA.getProjectInfo().getTags(), project.getProjectInfo().getTags());
                Assert.assertEquals(projectA.getProjectDetails(), project.getProjectDetails());
                Assert.assertEquals(expectedSimpleTree(), project.getFileTree().getSimpleTree());
            }
        }
    }

    @Test
    public void shouldGenerateReportAndWriteItToFile() {
        //given
        File projectsLocation = new File("src/test/resources/dummyProjects");
        ProNotesHtmlSimple proNotes = new ProNotesHtmlSimple(projectsLocation);
        proNotes.findProjects();

        //when
        String report = proNotes.generateReport();
        proNotes.writeReport();

        //then
        Assert.assertEquals("", report);
        Assert.assertEquals("", report);
    }

    @Test
    public void shouldGenerateReportForSeveralInputs() {
        //given
        String[] args = new String[2];
        args[0] = "src/test/resources/dummyProjects/task001_jira123_my_first";
        args[1] = "src/test/resources/dummyProjects/task003_other";

        //when
        RunApp.main(args);

        //then
        Assert.assertEquals("", "report");
    }

    private String expectedSimpleTree() {
        return "task001_jira123_my_first\n" +
                " +other\n" +
                " +REPO\n" +
                "  +my-project\n" +
                "   +src\n" +
                "    +main\n" +
                "     +java\n" +
                "      -MY_REPO.java\n" +
                "     -MY_REPO2.java\n" +
                "     -MY_REPO3.java\n" +
                " +SUPPORT\n" +
                "  -bugA.txt\n" +
                "  -details.txt\n" +
                "  -info_conception_A.txt\n" +
                "  -problem-a-described.png\n" +
                "  -problem-b.gif\n" +
                "  -_details.txt\n" +
                " -_info.txt";
    }

    private Project getExampleProjectA() {
        File projectLocation = new File("./src/test/resources/dummyProjects/task001_jira123_my_first");
        Project projectA = new FolderProject(projectLocation);
        projectA.setDescription("In this project I made several important notes. See pictures. There were alse several problems, like:\n" +
                "1. Problem with a,\n" +
                "2. Problem with b,\n" +
                "3. Problem with c.\n" +
                "\n" +
                "Tom helped me with problem 1 and 2. Jan helped me with problem 3. \n" +
                "This can be also found in http://stackoverflow.com/my-problem-a.\n" +
                "\n" +
                "Solution description is in pictures - see below. Click on links for more info.");

        ProjectInfo projectInfo = new SimpleProjectInfo();
        projectInfo.setDate("2023-02-11");
        projectInfo.setAuthor("M.Rys");
        projectInfo.setTags("Jira123,123,my,my-project-a,problem a");
        projectInfo.setJira("Jira123");
        projectA.setProjectInfo(projectInfo);

        File projectDetailsLocation = new File("./src/test/resources/dummyProjects/task001_jira123_my_first/SUPPORT/_details.txt");
        ProjectDetails projectDetails = new SimpleProjectDetailsForHtml(projectDetailsLocation);
        projectDetails.addTxt("Conception for A", "info_conception_A.txt");
        projectDetails.addImg("Problem description", "problem-b.gif", "width=100");
        projectDetails.addTxt("Bug list", "bugA.txt");
        projectA.addProjectDetails(projectDetails);

        return projectA;
    }
}