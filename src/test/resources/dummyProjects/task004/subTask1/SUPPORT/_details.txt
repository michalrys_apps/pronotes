-- EXAMPLE OF USAGE
- #txt#,#TITLE#,#FILENAME#
- #img#,#TITLE#,#FILENAME#
- #img#,#TITLE#,#FILENAME#,#width=100#
- #img#,#TITLE#,#FILENAME#,#height=100#
- #img#,#TITLE#,#FILENAME#,#width=100 height=100#
---------------------------------------------------
#txt#,#Conception for A#,#info_conception_A.txt#
#img#,#Problem B#,#problem-b.gif#,#width=100#