package com.michalrys.pronotes.pronotes.files;

import com.michalrys.pronotes.pronotes.project.Project;

import java.io.File;

public interface ProjectDetailsParser {
    void readAndSet(Project project, File file);
}
