package com.michalrys.pronotes.pronotes.files;

import java.io.File;

public interface Reader {
    String read(File file);
}
