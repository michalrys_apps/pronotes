package com.michalrys.pronotes.pronotes.files;

import com.michalrys.pronotes.pronotes.project.Project;
import com.michalrys.pronotes.pronotes.project.details.ProjectDetails;
import com.michalrys.pronotes.pronotes.project.details.SimpleProjectDetailsForHtml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SimpleProjectDetailsParser implements ProjectDetailsParser {

    @Override
    public void readAndSet(Project project, File file) {
        ProjectDetails projectDetails = new SimpleProjectDetailsForHtml(file);

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.matches("^-.*")) {
                   continue;
                }
                if (line.matches("^#txt#.*")) {
                    String[] parameters = line.split("#,#");
                    String title = parameters[1].replaceAll("#", "");
                    String fileName = parameters[2].replaceAll("#", "");
                    projectDetails.addTxt(title, fileName);
                    continue;
                }
                if (line.matches("^#img#.*")) {
                    String[] parameters = line.split("#,#");
                    String title = parameters[1].replaceAll("#", "");
                    String fileName = parameters[2].replaceAll("#", "");
                    String imgSrcSizeCode = parameters[3].replaceAll("#", "");
                    projectDetails.addImg(title, fileName, imgSrcSizeCode);
                    continue;
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        project.addProjectDetails(projectDetails);
    }
}
