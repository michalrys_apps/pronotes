package com.michalrys.pronotes.pronotes.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class DetailsTXTreader implements Reader {
    @Override
    public String read(File file) {
        StringBuilder results = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                results.append(line + "<br/>\n");
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        results.replace(results.length() - 1, results.length(), "");
        return results.toString();
    }
}
