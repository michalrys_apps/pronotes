package com.michalrys.pronotes.pronotes.files;

import com.michalrys.pronotes.pronotes.project.Project;

import java.io.File;

public interface ProjectInfoParser {
    void readAndSet(Project project, File file);
}
