package com.michalrys.pronotes.pronotes.files;

import com.michalrys.pronotes.pronotes.project.Project;
import com.michalrys.pronotes.pronotes.project.info.SimpleProjectInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SimpleProjectInfoParser implements ProjectInfoParser {
    @Override
    public void readAndSet(Project project, File file) {
        SimpleProjectInfo projectInfo = new SimpleProjectInfo();
        StringBuilder description = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.matches("^Jira:.*")) {
                    line = line.replaceAll("Jira:", "");
                    projectInfo.setJira(line.trim());
                    continue;
                }
                if (line.matches("^Date:.*")) {
                    line = line.replaceAll("Date:", "");
                    projectInfo.setDate(line.trim());
                    continue;
                }
                if (line.matches("^Author:.*")) {
                    line = line.replaceAll("Author:", "");
                    projectInfo.setAuthor(line.trim());
                    continue;
                }
                if (line.matches("^Tags:.*")) {
                    line = line.replaceAll("Tags:", "");
                    projectInfo.setTags(line.trim());
                    continue;
                }
                if (line.matches("^-----.*")) {
                    continue;
                }
                description.append(line + "\n");
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        description.replace(description.length() - 1, description.length(), "");
        project.setDescription(description.toString());
        project.setProjectInfo(projectInfo);
    }
}

