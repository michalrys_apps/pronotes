package com.michalrys.pronotes.pronotes.files;

import java.io.File;

public interface Writer {
    void write(String content, String fileExtension, File directory);
}
