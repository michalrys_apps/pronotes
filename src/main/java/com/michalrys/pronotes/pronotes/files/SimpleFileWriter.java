package com.michalrys.pronotes.pronotes.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

public class SimpleFileWriter implements Writer {
    public static final String FILENAME_PREFIX = "proNotes_";
    public static final String FILENAME_TIMESTAMP_FORMAT = "YYYY-MM-dd_HH-mm-ss";
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public void write(String content, String fileExtension, File directory) {
        File file = generateFilePathToWrite(fileExtension, directory);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            if (content != null) {
                writer.write(content);
            } else {
                writer.write("no content");
            }
        } catch (IOException e) {
            logger.severe("Problem with writing to a file : " + file.getAbsolutePath());
            System.exit(0);
        }
        logger.info("Written in file: " + file.getName());
    }

    private File generateFilePathToWrite(String fileExtension, File directory) {
        String name = LocalDateTime.now().format(DateTimeFormatter.ofPattern(FILENAME_TIMESTAMP_FORMAT));
        return new File(directory.getAbsolutePath() + "/" + FILENAME_PREFIX + name + fileExtension);
    }
}
