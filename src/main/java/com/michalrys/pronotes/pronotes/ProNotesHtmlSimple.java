package com.michalrys.pronotes.pronotes;

import com.michalrys.pronotes.pronotes.files.SimpleFileWriter;
import com.michalrys.pronotes.pronotes.files.Writer;
import com.michalrys.pronotes.pronotes.project.Project;
import com.michalrys.pronotes.pronotes.project.finder.ProjectsFinder;
import com.michalrys.pronotes.pronotes.project.finder.SimpleProjectsFinder;
import com.michalrys.pronotes.pronotes.report.html.HtmlReport;
import com.michalrys.pronotes.pronotes.report.html.ReportGenerator;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

public class ProNotesHtmlSimple implements ProNotes {
    public static final String PROJECT_FILE = "_info.txt";
    public static final String DETAILS_FILE = "_details.txt";
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private final File projectsLocation;
    private final Writer writer = new SimpleFileWriter();
    private ProjectsFinder projectsFinder;
    private final ReportGenerator reportGenerator = new HtmlReport();
    private String report;
    private File partialReportLocation;

    public ProNotesHtmlSimple(File projectsLocation) {
        this.projectsLocation = projectsLocation;
        this.partialReportLocation = projectsLocation;
        projectsFinder = new SimpleProjectsFinder(projectsLocation, PROJECT_FILE, DETAILS_FILE);
        logger.info("Looking for projects in: " + projectsLocation.getAbsolutePath());
    }

    @Override
    public void setPartialReportLocation(File partialReportLocation) {
        this.partialReportLocation = partialReportLocation;
        projectsFinder = new SimpleProjectsFinder(partialReportLocation, PROJECT_FILE, DETAILS_FILE);
        logger.info("Looking for projects in: " + partialReportLocation.getAbsolutePath());
    }

    @Override
    public List<Project> findProjects() {
        logger.info("X projects were found.");
        projectsFinder.find();
        return projectsFinder.get();
    }

    @Override
    public String generateReport() {
        logger.info("Report X was generated.");
        this.report = reportGenerator.generate(projectsFinder.get());
        return report;
    }

    @Override
    public String generateFirstReport() {
        logger.info("Report X was generated.");
        this.report = reportGenerator.generateFirst(projectsFinder.get());
        return report;
    }

    @Override
    public String appendReport() {
        logger.info("Report X was generated.");
        this.report = reportGenerator.append(report, projectsFinder.get());
        return report;
    }

    @Override
    public String appendLastReport() {
        logger.info("Report X was generated.");
        this.report = reportGenerator.appendLast(report, projectsFinder.get());
        return report;
    }

    @Override
    public void writeReport() {
        logger.info("Writing report to a file...");
        writer.write(report, ".html", projectsLocation);
    }
}
