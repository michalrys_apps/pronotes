package com.michalrys.pronotes.pronotes.report.html;

import com.michalrys.pronotes.pronotes.files.Reader;
import com.michalrys.pronotes.pronotes.files.SimpleReader;
import com.michalrys.pronotes.pronotes.project.Project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class HtmlReport implements ReportGenerator {
    private final SimpleReader readerTopEnd = new SimpleReader();

    @Override
    public String generate(List<Project> projects) {
        StringBuilder report = new StringBuilder();
        setTop(report);

        for (Project project : projects) {
            addArticle(report, project);
        }

        setEnd(report);
        return report.toString();
    }

    @Override
    public String generateFirst(List<Project> projects) {
        StringBuilder report = new StringBuilder();
        setTop(report);

        for (Project project : projects) {
            addArticle(report, project);
        }

        return report.toString();
    }

    @Override
    public String append(String report, List<Project> projects) {
        StringBuilder reportAppended = new StringBuilder();
        reportAppended.append(report);

        for (Project project : projects) {
            addArticle(reportAppended, project);
        }

        return reportAppended.toString();
    }

    @Override
    public String appendLast(String report, List<Project> projects) {
        StringBuilder reportAppended = new StringBuilder();
        reportAppended.append(report);

        for (Project project : projects) {
            addArticle(reportAppended, project);
        }

        setEnd(reportAppended);
        return reportAppended.toString();
    }

    private void addArticle(StringBuilder report, Project project) {
        InputStream input = this.getClass().getClassLoader().getResourceAsStream("template1article.html");
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains("#PROJECT_LOCATION#")) {
                    String absolutePath = project.getLocation().getAbsolutePath();
                    line = line.replaceAll("#PROJECT_LOCATION#", absolutePath.replace("\\", "/"));
                }
                if (line.contains("#PROJECT_TITLE#")) {
                    String title = project.getTitle();
                    line = line.replaceAll("#PROJECT_TITLE#", title);
                }
                if (line.contains("#PROJECT_DATE#")) {
                    String date = project.getProjectInfo().getDate();
                    line = line.replaceAll("#PROJECT_DATE#", date);
                }
                if (line.contains("#PROJECT_AUTHOR#")) {
                    String author = project.getProjectInfo().getAuthor();
                    line = line.replaceAll("#PROJECT_AUTHOR#", author);
                }
                if (line.contains("#PROJECT_TAGS#")) {
                    String tags = project.getProjectInfo().getTags();
                    line = line.replaceAll("#PROJECT_TAGS#", tags);
                }
                if (line.contains("#PROJECT_FILE_TREE#")) {
                    line = project.getFileTree().getTree();
                }
                if (line.contains("#PROJECT_DESCRIPTION#")) {
                    String description = project.getDescription();
                    line = line.replaceAll("#PROJECT_DESCRIPTION#", description);
                }
                if (line.contains("#PROJECT_DETAILS#")) {
                    String projectDetails = project.getProjectDetails();
                    if (!projectDetails.equals("")) {
                        line = line.replace("#PROJECT_DETAILS#", "<hr><br/>" + projectDetails);
                    } else {
                        line = "";
                    }
                }
                report.append(line + "\n");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void setTop(StringBuilder report) {
        InputStream input = this.getClass().getClassLoader().getResourceAsStream("template1top.html");
        File file = new File("src/main/java/com/michalrys/pronotes/pronotes/report/html/template1top.html");
        String read = readerTopEnd.read(input);
        report.append(read);
    }

    private void setEnd(StringBuilder report) {
        InputStream input = this.getClass().getClassLoader().getResourceAsStream("template1end.html");

        String read = readerTopEnd.read(input);
        if (read.contains("#FOOTER_DATE#")) {
            LocalDateTime now = LocalDateTime.now();
            String dateTime = now.format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss"));
            read = read.replaceAll("#FOOTER_DATE#", dateTime);
        }
        if (read.contains("#FOOTER_USER#")) {
            String user = System.getenv("username");
            read = read.replaceAll("#FOOTER_USER#", user);
        }
        report.append(read);
    }
}
