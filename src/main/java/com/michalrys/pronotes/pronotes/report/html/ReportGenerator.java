package com.michalrys.pronotes.pronotes.report.html;

import com.michalrys.pronotes.pronotes.project.Project;

import java.util.List;

public interface ReportGenerator {
    String generate(List<Project> projects);

    String generateFirst(List<Project> projects);

    String append(String report, List<Project> projects);

    String appendLast(String report, List<Project> projects);
}
