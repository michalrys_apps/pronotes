package com.michalrys.pronotes.pronotes;

import com.michalrys.pronotes.pronotes.project.Project;

import java.io.File;
import java.util.List;

public interface ProNotes {
    List<Project> findProjects();

    String generateReport();

    String generateFirstReport();

    String appendReport();

    String appendLastReport();

    void writeReport();

    void setPartialReportLocation(File mainLocation);
}
