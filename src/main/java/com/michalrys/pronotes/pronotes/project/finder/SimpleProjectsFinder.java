package com.michalrys.pronotes.pronotes.project.finder;

import com.michalrys.pronotes.pronotes.files.ProjectDetailsParser;
import com.michalrys.pronotes.pronotes.files.ProjectInfoParser;
import com.michalrys.pronotes.pronotes.files.SimpleProjectDetailsParser;
import com.michalrys.pronotes.pronotes.files.SimpleProjectInfoParser;
import com.michalrys.pronotes.pronotes.project.FolderProject;
import com.michalrys.pronotes.pronotes.project.Project;
import com.michalrys.pronotes.pronotes.project.item.Item;
import com.michalrys.pronotes.pronotes.project.item.SimpleItem;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SimpleProjectsFinder implements ProjectsFinder {
    private final File projectsLocation;
    private final String projectFileName;
    private final String detailsFileName;
    private List<Project> projects = new LinkedList<>();
    private final ProjectInfoParser projectInfoParser = new SimpleProjectInfoParser();
    private final ProjectDetailsParser projectDetailsParser = new SimpleProjectDetailsParser();

    public SimpleProjectsFinder(File projectsLocation, String projectDefinitionFileName, String projectDetailsFileName) {
        this.projectsLocation = projectsLocation;
        projectFileName = projectDefinitionFileName;
        detailsFileName = projectDetailsFileName;
    }

    @Override
    public void find() {
        checkFiles(projectsLocation.listFiles());
    }

    private void checkFiles(File[] files) {
        if (files == null) {
            return;
        }
        if (files.length == 0) {
            return;
        }
        for (File file : files) {
            if (file.isFile()) {
                String name = file.getName();
                if (name.equals(projectFileName)) {
                    File parentFile = file.getParentFile();
                    Project project = new FolderProject(parentFile);
                    projectInfoParser.readAndSet(project, file);

                    Item fileTree = project.getFileTree();
                    checkSubFilesOfProject(project, fileTree, parentFile.listFiles());

                    projects.add(project);
                }
            } else {
                checkFiles(file.listFiles());
            }
        }
    }

    private void checkSubFilesOfProject(Project project, Item item, File[] filesInProject) {
        if (filesInProject == null) {
            return;
        }
        if (filesInProject.length == 0) {
            return;
        }
        List<File> excludedFolders = new ArrayList<>();
        for (File fileProject : filesInProject) {
            if (fileProject.isFile()) {
                if (fileProject.getName().equals("!_exclude")) {
                    File parentFileToExclude = fileProject.getParentFile();
                    excludedFolders.add(parentFileToExclude);
                }
                SimpleItem fileSubItem = new SimpleItem(fileProject);
                fileSubItem.increaseLevel(item.getLevel() + 1);
                item.addToSubItem(fileSubItem);
                String fileInProjectName = fileProject.getName();
                if (fileInProjectName.equals(detailsFileName)) {
                    projectDetailsParser.readAndSet(project, fileProject);
                }
            } else {
                SimpleItem folderItem = new SimpleItem(fileProject);
                folderItem.increaseLevel(item.getLevel() + 1);

                File parentFile = fileProject.getParentFile();
                if (!excludedFolders.contains(parentFile)) {
                    checkSubFilesOfProject(project, folderItem, fileProject.listFiles());
                }
                item.addToSubItem(folderItem);
            }
        }
    }

    @Override
    public List<Project> get() {
        return projects;
    }
}
