package com.michalrys.pronotes.pronotes.project;

import com.michalrys.pronotes.pronotes.project.details.ProjectDetails;
import com.michalrys.pronotes.pronotes.project.info.ProjectInfo;
import com.michalrys.pronotes.pronotes.project.item.Item;

import java.io.File;

public interface Project {
    String DEFAULT_AUTHOR = "M.Rys";

    String getTitle();

    void setProjectInfo(ProjectInfo projectInfo);

    ProjectInfo getProjectInfo();

    File getLocation();

    void setDescription(String description);

    String getDescription();

    void addProjectDetails(ProjectDetails projectDetails);

    String getProjectDetails();

    Item getFileTree();
}
