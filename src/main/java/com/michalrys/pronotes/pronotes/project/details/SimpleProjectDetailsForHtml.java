package com.michalrys.pronotes.pronotes.project.details;

import com.michalrys.pronotes.pronotes.files.DetailsTXTreader;
import com.michalrys.pronotes.pronotes.files.Reader;

import java.io.File;

public class SimpleProjectDetailsForHtml implements ProjectDetails {
    private final File location;
    private final Reader txtReader = new DetailsTXTreader();
    private final StringBuilder details = new StringBuilder();

    public SimpleProjectDetailsForHtml(File projectDetails) {
        this.location = new File(projectDetails.getParent());
    }

    @Override
    public void addTxt(String title, String fileName) {
        details.append("\n");
        details.append("<b><u>");
        details.append(title);
        details.append("</b></u>");
        details.append("<br/>\n");
        File fileToRead = getFile(fileName);
        String content = "-> for " + fileToRead.getAbsolutePath() + "<br/>\n";
        if (fileToRead.exists() && fileToRead.isFile()) {
            content = txtReader.read(fileToRead);
        }
        details.append(content);
    }

    @Override
    public void addImg(String title, String fileName, String imgSrcSizeCode) {
        details.append("\n");
        details.append("<b><u>");
        details.append(title);
        details.append("</b></u>");
        details.append("<br/>\n");
        File imgFile = getFile(fileName);
        String filePath = imgFile.getAbsolutePath();
        filePath = filePath.replaceAll("\\\\", "/");
        details.append("<a href=\"file:///");
        details.append(filePath);
        details.append("\" target=\"_blank\">");
        details.append("<img src=\"file:///");
        details.append(filePath);
        details.append("\" ");
        details.append(imgSrcSizeCode);
        details.append("></a><br/>\n");
    }

    @Override
    public String getContent() {
        return details.toString();
    }

    private File getFile(String fileName) {
        String absolutePath = location.getAbsolutePath();
        return new File(absolutePath + "/" + fileName);
    }
}
