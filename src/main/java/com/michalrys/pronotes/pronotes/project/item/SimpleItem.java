package com.michalrys.pronotes.pronotes.project.item;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class SimpleItem implements Item {
    private final File location;
    private final String link;
    private final List<Item> subItems = new LinkedList<>();
    private int level = 0;

    public SimpleItem(File location) {
        this.location = location;
        this.link = location.getAbsolutePath();
    }

    @Override
    public void addToSubItem(Item item) {
        subItems.add(item);
    }

    @Override
    public List<Item> getSubItems() {
        return subItems;
    }

    @Override
    public File getLocation() {
        return location;
    }

    @Override
    public String getLink() {
        return link;
    }

    @Override
    public void increaseLevel(int amount) {
        level += amount;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public String getSimpleTree() {
        StringBuilder results = new StringBuilder();
        results.append(location.getName() + "\n");

        results = getTreeNames(results, this);
        results.replace(results.length() - 1, results.length(), "");
        return results.toString();
    }

    private StringBuilder getTreeNames(StringBuilder results, Item item) {
        if (item.getSubItems() == null) {
            return results;
        }
        if (item.getSubItems().size() == 0) {
            return results;
        }
        for (Item subItem : item.getSubItems()) {
            for (int i = 0; i <= item.getLevel(); i++) {
                results.append(" ");
            }
            File file = subItem.getLocation();
            if (file.isDirectory()) {
                results.append("+");
            } else {
                results.append("-");
            }
            results.append(subItem.getLocation().getName());
            results.append("\n");
            getTreeNames(results, subItem);
        }
        return results;
    }

    @Override
    public String getTree() {
        StringBuilder results = new StringBuilder();
        results.append("&#128193;");
        results.append("<a href=\"file:///");
        String path = location.getAbsolutePath();
        results.append(path.replaceAll("\\\\", "/"));
        results.append("\" target=\"_blank\"><b>");
        results.append(location.getName());
        results.append("</b></a></br>\n");

        results = getTreeNamesHtml(results, this);
        results.replace(results.length() - 1, results.length(), "");
        return results.toString();

    }

    private StringBuilder getTreeNamesHtml(StringBuilder results, Item item) {
        if (item.getSubItems() == null) {
            return results;
        }
        if (item.getSubItems().size() == 0) {
            return results;
        }
        for (Item subItem : item.getSubItems()) {
            for (int i = 0; i <= item.getLevel(); i++) {
                results.append("&nbsp;&nbsp;&nbsp;&nbsp;");
            }
            File file = subItem.getLocation();
            if (file.isDirectory()) {
                results.append("&#128193;");
            } else {
                results.append("&#128462;");
            }
            String name = subItem.getLocation().getName();
            String path = subItem.getLocation().getAbsolutePath();
            path = path.replaceAll("\\\\", "/");
            results.append("<a href=\"file:///");
            results.append(path);
            results.append("\" target=\"_blank\">");
            results.append(name);
            results.append("</a><br/>\n");
            getTreeNamesHtml(results, subItem);
        }
        return results;
    }

    @Override
    public String toString() {
        return location.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleItem that = (SimpleItem) o;

        return link != null ? link.equals(that.link) : that.link == null;
    }

    @Override
    public int hashCode() {
        return link != null ? link.hashCode() : 0;
    }
}
