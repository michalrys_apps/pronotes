package com.michalrys.pronotes.pronotes.project.details;

public interface ProjectDetails {
    void addTxt(String title, String fileName);

    void addImg(String title, String fileName, String imgSrcSizeCode);

    String getContent();
}
