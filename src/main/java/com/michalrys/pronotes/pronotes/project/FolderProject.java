package com.michalrys.pronotes.pronotes.project;

import com.michalrys.pronotes.pronotes.project.details.ProjectDetails;
import com.michalrys.pronotes.pronotes.project.info.ProjectInfo;
import com.michalrys.pronotes.pronotes.project.item.Item;
import com.michalrys.pronotes.pronotes.project.item.SimpleItem;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class FolderProject implements Project {
    private final File location;
    private final String title;
    private final Item fileTree;
    private String description = "";
    private ProjectInfo projectInfo;
    private List<ProjectDetails> projectDetails = new LinkedList<>();

    public FolderProject(File location) {
        this.location = location;
        if (location.isDirectory()) {
            this.title = location.getName();
        } else {
            throw new RuntimeException("Project is not a folder !");
        }
        this.fileTree = new SimpleItem(location);
    }

    @Override
    public File getLocation() {
        return location;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setProjectInfo(ProjectInfo projectInfo) {
        this.projectInfo = projectInfo;
    }

    @Override
    public ProjectInfo getProjectInfo() {
        return projectInfo;
    }

    @Override
    public void addProjectDetails(ProjectDetails projectDetails) {
        this.projectDetails.add(projectDetails);
    }

    @Override
    public String getProjectDetails() {
        StringBuilder results = new StringBuilder();
        for (ProjectDetails details : projectDetails) {
            results.append(details.getContent());
        }
        return results.toString();
    }

    @Override
    public Item getFileTree() {
        return fileTree;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FolderProject that = (FolderProject) o;

        return title != null ? title.equals(that.title) : that.title == null;
    }

    @Override
    public int hashCode() {
        return title != null ? title.hashCode() : 0;
    }

    @Override
    public String toString() {
        return title;
    }
}