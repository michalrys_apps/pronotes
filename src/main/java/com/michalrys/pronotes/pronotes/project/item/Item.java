package com.michalrys.pronotes.pronotes.project.item;

import java.io.File;
import java.util.List;

public interface Item {
    void addToSubItem(Item item);

    List<Item> getSubItems();

    File getLocation();

    String getLink();

    void increaseLevel(int amount);

    int getLevel();

    String getSimpleTree();

    String getTree();
}
