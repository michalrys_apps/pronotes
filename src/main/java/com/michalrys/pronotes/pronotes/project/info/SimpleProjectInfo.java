package com.michalrys.pronotes.pronotes.project.info;

import com.michalrys.pronotes.pronotes.project.Project;

public class SimpleProjectInfo implements ProjectInfo {
    private String jira = "";
    private String author = Project.DEFAULT_AUTHOR;
    private String date = "";
    private String tags = "";

    @Override
    public void setJira(String jira) {
        this.jira = jira;
    }

    @Override
    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public void setTags(String tags) {
        this.tags = tags;
    }

    @Override
    public String getJira() {
        return jira;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public String getTags() {
        return tags;
    }

    @Override
    public String toString() {
        return "SimpleProjectInfo{" +
                "jira='" + jira + '\'' +
                ", author='" + author + '\'' +
                ", date='" + date + '\'' +
                ", tags='" + tags + '\'' +
                '}';
    }
}
