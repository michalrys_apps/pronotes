package com.michalrys.pronotes.pronotes.project.finder;

import com.michalrys.pronotes.pronotes.project.Project;

import java.util.List;

public interface ProjectsFinder {
    void find();

    List<Project> get();
}
