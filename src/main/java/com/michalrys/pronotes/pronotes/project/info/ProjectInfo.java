package com.michalrys.pronotes.pronotes.project.info;

public interface ProjectInfo {
    void setDate(String date);

    void setAuthor(String author);

    void setTags(String tags);

    void setJira(String jira);

    String getJira();

    String getDate();

    String getAuthor();

    String getTags();
}
