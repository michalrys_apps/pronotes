package com.michalrys.pronotes;

import com.michalrys.pronotes.pronotes.ProNotes;
import com.michalrys.pronotes.pronotes.ProNotesHtmlSimple;

import java.io.File;
import java.time.LocalDateTime;
import java.util.logging.Logger;

public class RunApp {
    private final static Logger logger = Logger.getLogger(RunApp.class.getName());

    public static void main(String[] args) {
        logger.info("Started : " + LocalDateTime.now() + ", by " + System.getenv("username"));
        if (args.length <= 1) {
            File mainLocation = RunAppUtils.getLocation(args);
            ProNotes proNotes = new ProNotesHtmlSimple(mainLocation);
            proNotes.findProjects();
            proNotes.generateReport();
            proNotes.writeReport();
        } else {
            File mainLocationForReport = RunAppUtils.getLocation(args);
            ProNotes proNotes = new ProNotesHtmlSimple(mainLocationForReport.getParentFile());

            for (int i = 0; i < args.length; i++) {
                String arg = args[i];
                File mainLocation = RunAppUtils.getLocation(arg);
                proNotes.setPartialReportLocation(mainLocation);
                proNotes.findProjects();

                if (i == 0) {
                    proNotes.generateFirstReport();
                } else if (i < args.length - 1) {
                    proNotes.appendReport();
                } else if (i == args.length - 1) {
                    proNotes.appendLastReport();
                    proNotes.writeReport();
                }
            }
        }
        logger.info("Ended : " + LocalDateTime.now());
    }
}
