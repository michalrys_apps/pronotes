package com.michalrys.pronotes;

import java.io.File;
import java.util.Scanner;
import java.util.logging.Logger;

final class RunAppUtils {
    private final static Logger logger = Logger.getLogger(RunAppUtils.class.getName());

    public static File getLocation(String[] inputArgs) {
        if (inputArgs != null && inputArgs.length > 0) {
            String input = inputArgs[0];
            File file = new File(input);
            if (file.exists()) {
                return file.isFile() ? file.getParentFile() : file;
            }
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Absolute path to folder with projects:");
        String input = scanner.nextLine();
        File file = new File(input);
        if (file.exists()) {
            return file.isFile() ? file.getParentFile() : file;
        }
        logger.severe("Wrong path to folder: " + file.getAbsolutePath());
        System.exit(0);
        return null;
    }

    public static File getLocation(String inputArg) {
        File file = new File(inputArg);
        if (file.exists()) {
            return file.isFile() ? file.getParentFile() : file;
        }
        logger.severe("Wrong path to folder: " + file.getAbsolutePath());
        System.exit(0);
        return null;
    }
}
